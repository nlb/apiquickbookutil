import codecs
import os

from setuptools import setup, find_packages


def read(*parts):
    filename = os.path.join(os.path.dirname(__file__), *parts)
    with codecs.open(filename, encoding='utf-8') as fp:
        return fp.read()


VERSION = (0, 2, 0)
version = '.'.join(map(str, VERSION))

setup(
    name='apiquickbookutil',
    version=version,
    author='Nilberto Rodriguez',
    author_email='nilbertrr@gmail.com',
    description='A Python library for accessing the Quickbooks API.',
    url='https://github.com/sidecars/python-quickbooks',
    license='MIT',
    keywords=['quickbooks', 'util', 'accounting'],
    # long_description=read('.rst'),

    entry_points={
        # 'console_scripts': ['quickbooks-cli=quickbooks.tools.cli:cli_execute']
    },

    install_requires=[
        'setuptools',
        'rauth>=0.7.1',
        'requests>=2.7.0',
        'simplejson>=2.2.0',
        'six>=1.4.0',
        'python-dateutil',
        'python-quickbooks==0.7.3',
    ],

    classifiers=[
        'Development Status :: 1 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    packages=find_packages(),
)

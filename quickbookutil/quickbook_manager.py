import tempfile
import os
import logging

from quickbooks import Oauth2SessionManager, QuickBooks
from quickbooks.objects import DetailLine, DescriptionLine, AccountBasedExpenseLine, AccountBasedExpenseLineDetail, \
    ItemBasedExpenseLine, ItemBasedExpenseLineDetail,Account
from quickbooks.objects.bill import Bill
from quickbooks.objects.vendor import Vendor
from quickbooks.objects.base import Ref, AttachableRef, Address
import requests
from quickbooks.objects.attachable import Attachable

logger = logging.getLogger(__name__)


class QuickBookManager(object):
    def __init__(self, **kwargs):
        self.client_id = ''
        self.client_secret = ''
        self.callback_url = ''
        self.sandbox = False
        self.client = None
        self.access_token = ''
        self.refresh_token = ''
        self.default_account='Uncategorized Expense'
        if 'client_id' in kwargs:
            self.client_id = kwargs['client_id']
        if 'client_secret' in kwargs:
            self.client_secret = kwargs['client_secret']
        # self.client_secret = client_secret
        if 'callback_url' in kwargs:
            self.callback_url = kwargs['callback_url']
        if 'sandbox' in kwargs:
            self.sandbox = kwargs['sandbox']
            # self.callback_url = callback_url
        # self.sandbox = sandbox
        self.client = None
        self.access_token = ''
        self.refresh_token = ''

        # session_manager = Oauth2SessionManager(
        #     sandbox=self.sandbox,
        #     client_id=self.client_id,
        #     client_secret=self.client_secret,
        #     base_url=self.callback_url
        # )
        #

    def get_authorize_url(self):
        session_manager = Oauth2SessionManager(
            sandbox=self.sandbox,
            client_id=self.client_id,
            client_secret=self.client_secret,
            base_url=self.callback_url
        )
        # self.callback_url = 'http://localhost:8000'  # Quickbooks will send the response to this url
        authorize_url = session_manager.get_authorize_url(self.callback_url)
        # result = requests.get(authorize_url)
        return authorize_url

    # code=request.GET['code']
    def get_access_token(self, code):
        session_manager = Oauth2SessionManager(
            sandbox=self.sandbox,
            client_id=self.client_id,
            client_secret=self.client_secret,
            base_url=self.callback_url,
        )

        result = session_manager.get_access_tokens(code)

        self.access_token = session_manager.access_token
        if self.access_token == '':
            return result

        return session_manager
        # save token in some storage

    def get_new_access_token(self, refresh_token):
        session = Oauth2SessionManager(
            sandbox=self.sandbox,
            client_id=self.client_id,
            client_secret=self.client_secret,
            base_url=self.callback_url
        )

        r = session.get_new_access_tokens(refresh_token)
        if r:
            return r

        self.access_token = session.access_token
        self.refresh_token = session.refresh_token
        return self.access_token

    def create_client(self, realm_id):

        try:
            session_manager = Oauth2SessionManager(
                sandbox=self.sandbox,
                client_id=self.client_id,
                client_secret=self.client_secret,
                access_token=self.access_token,
            )
            self.client = QuickBooks(
                sandbox=self.sandbox,
                session_manager=session_manager,
                company_id=realm_id
            )
            return self.client
        except Exception as e:
            logger.error(e)

            return None


    def get_account(self,account):
        """

        :param account:
        :return:
        """
        try:
            account_result=Account.filter(Name=account,qb=self.client)
            return account_result
        except Exception as e:
            logger.error(e)

    def get_bill(self, dte_folio):
        try:
            bill = Bill.filter(DocNumber=str(dte_folio), qb=self.client, max_results='1')
            return bill
        except Exception as e:
            logger.error(e)

    def create_bill(self, vendor, fch_emission, dte_folio, dte_total_amount, description='', account_number=1):
        """

        :param vendor: Vendedor Id
        :param fch_emission: Fecha de emision del Dte
        :param dte_folio: Folio Dte
        :param dte_total_amount: Monto Total Dte
        :param description
        :param account_number
        :return:
        """
        try:
            bill = Bill()
            bill.VendorRef = vendor.to_ref()
            bill.DocNumber = str('FE-{}'.format(dte_folio))
            bill.DueDate = fch_emission
            # line
            line = AccountBasedExpenseLine()
            line.Amount = dte_total_amount
            line.DetailType = "AccountBasedExpenseLineDetail"
            line.Description = description

            account_ref = Ref()
            account_ref.type = "Account"
            account_ref.value = account_number
            line.AccountBasedExpenseLineDetail = AccountBasedExpenseLineDetail()
            line.AccountBasedExpenseLineDetail.AccountRef = account_ref

            # other line
            # line1=ItemBasedExpenseLine()
            # line1.Amount=dte_total_amount
            # line1.DetailType="ItemBasedExpenseLineDetail"
            # line1.ItemBasedExpenseLineDetail=ItemBasedExpenseLineDetail()
            #


            bill.Line.append(line)
            # bill.PrivateNote=description

            bill.save(qb=self.client)

            return bill
        except Exception as e:
            logger.error(e)

    def get_vendor(self, id):

        try:
            vendor = Vendor.filter(Id=id, qb=self.client)
        except Exception as e:
            logger.error(e)

    def get_vendor_by_rut(self, rut):
        """
        Get Vendor by Display Name query
        :param name:
        :return:
        """
        try:
            vendor = Vendor.where("DisplayName LIKE '%({})%'".format(rut), max_results=1, qb=self.client)
            # vendor = Vendor.filter(CompanyName=name, qb=self.client)
            return vendor
        except Exception as e:
            logger.error(e)

    def create_vendor(self, display_name, company_name, taxid_number, address,comuna ='',ciudad='', acct_num='Uncategorized Expense'):
        """
         Crea un nuevo Vendedor
        :param display_name: with this format=>razon social(rut)
        :param company_name: compannia
        :param taxid_number: rut
        :param address:
        :param acct_num:
        :return: Vendor Obj.
        """
        try:
            vendor = Vendor()
            vendor.DisplayName = display_name
            vendor.CompanyName = company_name
            vendor.TaxIdentifier = taxid_number
            # vendor.BillAddr = address
            vendor.AcctNum = acct_num
            vendor.BillAddr = Address()
            vendor.BillAddr.Line1 = comuna
            vendor.BillAddr.Line2 = address
            vendor.BillAddr.City = ciudad
            vendor.BillAddr.Country = "Chile"
            vendor.BillAddr.CountrySubDivisionCode = "CL"


            r=vendor.save(qb=self.client)
            return vendor
        except Exception as e:
            logger.error(e)

    def create_attachable(self, entity, file_path):
        """
        Create attachable for Vendor
        :param entity: ej vendor.to_ref()
        :param file_path: File Uri ej:'/tmp/myfile.pdf'
        :return:
        """
        try:
            attachable = Attachable()
            # test_file = tempfile.NamedTemporaryFile(suffix=".txt")

            # vendor = Vendor.filter(CompanyName=rut, qb=self.client)[0]

            attachable_ref = AttachableRef()
            # attachable_ref.EntityRef = vendor.to_ref()
            attachable_ref.EntityRef = entity.to_ref()
            attachable.AttachableRef.append(attachable_ref)

            attachable.FileName = os.path.basename(file_path)
            attachable._FilePath = file_path
            attachable.ContentType = 'application/pdf'

            attachable.save(qb=self.client)
            return attachable
        except Exception as e:
            logger.error(e)
